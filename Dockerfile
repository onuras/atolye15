FROM node:16 as builder
WORKDIR /app
COPY . .
RUN yarn install && yarn build

FROM node:16
WORKDIR /app
COPY --from=builder /app/package.json /app/dist /app/
RUN yarn install --prod

USER node
EXPOSE 3000
CMD node main.js
